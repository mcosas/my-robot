## System Requirements
1. JAVA_HOME must be setup to point at the home directory for Java (JDK or JRE).  
*Windows example: JAVA_HOME=C:\Program Files\Java\jdk1.8*  
*UNIX, Linux, Mac OS example: JAVA_HOME=/usr/local/jdk1.8*  
2. PATH must include the path to the bin of the Java home directory (JAVA_HOME) that includes java.exe for starting the Java virtual Machine (JVM).  
*Windows example: PATH=%PATH%;%JAVA_HOME%*  
*UNIX, Linux, Mac OS example: PATH=${PATH}:${JAVA_HOME}/bin*  

## Installing Robocode from the command line

1.   Copy https://gitlab.com/mcosas/my-robot/blob/master/install/robocode-1.9.3.0-setup.jar from Gitlab install folder to your computer
2.   Open a shell.
On Windows, search for "Command Prompt" in the Start Menu, and run it.
On macOS, search for "Terminal" in Spotlight, and run it.
3.   Type and enter (replacing robocode-a.b.c.d-setup.jar with the filename of the Robocode installer):
java -jar robocode-a.b.c.d-setup.jar
4.   The installer should run. If instead the previous command errors with 'java' is not recognized as an internal..., you have not set %PATH% correctly. \

## Running Robocode from the command line

./robocode.sh

## Creating Robot

1.     Open menu : Robot –> Source Editor
2.     Select Menu : File –> New -> Robot
3.     Enter the name of your robot : Example “MyTeamName”
4.     Package name : “wealth”
5.     Remember to Save your Robot and compile. ( menu options )
6.     API documentation : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

## Testing Robot

1.     Go to main menu : Battle -> New
2.     Select Menu : File – New - Robot
3.     Select the Robots you want to play against. Remember your Robot is under wealth
4.     Select : next an configure the battle field size to 1400x800
5.     Select : start battle to test your robot

## Package Robot – Necessary for the main battle

1.     Open menu : Robot –> Package Robot or team
2.     Select your Robot and press add and next
3.     Update the version
4.     Save the file in the robot folder
5.     Click Package
6.     Upload the new generated  jar file from robot folder into the gitlab folder robots

## Strategy

Move, radar and fire bullets

## Robot Anatomy

* Body ‒ Carries the gun with the radar on top. The body is used for moving the robot ahead and back, as well as turning left or right.
* Gun ‒ Mounted on the body and is used for firing energy bullets. The gun can turn left or right. Carries the radar on top.
* Radar ‒ Mounted on the gun and is used to scan for other robots when moved. The radar can turn left or right. The radar generates onScannedRobot() events when robots are detected.


## More Info
http://robowiki.net/wiki/Robocode/Getting_Started  
http://robowiki.net/wiki/Robocode/FAQ